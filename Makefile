run:
	docker-compose -f docker-compose.prod.yml up -d --build

stop:  # Stops all prod containers
	docker-compose -f docker-compose.prod.yml stop

# run_dev:  # Rebuilds the containers and runs them
# 	docker-compose up -d --build

test:
	docker-compose -f docker-compose.prod.yml exec web python manage.py test --settings conf.settings_test

tests: test

collectstatic_prod:
	docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear

migrate:
	docker-compose -f docker-compose.prod.yml exec web python manage.py migrate 

makemigrations:
	docker-compose -f docker-compose.prod.yml exec web python manage.py makemigrations 

rebuild_prod:
	docker-compose -f docker-compose.prod.yml down -v
	docker-compose -f docker-compose.prod.yml up -d --build
	docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput

# migrate:
# 	docker-compose exec web python manage.py migrate --noinput

urls:
	docker-compose -f docker-compose.prod.yml exec web python manage.py show_urls

shell:
	docker-compose -f docker-compose.prod.yml exec web python manage.py shell_plus

dbshell:  # Opens up PSQL shell
	docker-compose exec db psql --username=hello_django --dbname=hello_django_dev

volume_info:  # Shows docker volume information
	docker volume inspect django-on-docker_postgres_data

rm:  # Removes containers and their volumes
	docker-compose down -v

new_app:
	docker-compose exec web python manage.py startapp upload

load:
	docker-compose -f docker-compose.prod.yml exec web python manage.py loaddata data_2021_11_22.json

backup:  # TODO: add a backup capability
	pass
