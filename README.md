# Task.repeat_every and frequency are kinda dublicate fields.
#   repeat_every should be removed all together

# Add Vue component, which can update its state.
# Also add new components, base on list.

# Features
- See my week targets (parents should be ab)
    - page for everybodies goals
    - page for person to mark if his goal was achieved
    - way to add new goals

- Tags should have subtags (tag group)
- Recipe should have multiple tags
- Show tag subgroup as list with different indentation levels.
- Replace accordion with two column grid. First column shows the list, Second columns shows item details.

# Tech debt
- Upgrade Twitter bootstrap version
  - Make a single page with the newest bootstrap version, then migrate all other ones

# Todo
- [ ] Load a list of tasks from database using Django templates.
    - [ ] Filter the task list by user.

- [ ] VueJS integration:
    - [ ] Serve VueJS using Django: https://djangowaves.com/tutorial/how-to-use-vue-and-django/ https://djangowaves.com/tutorial/how-to-use-vue-and-django/
    - [ ] https://www.bezkoder.com/vue-3-authentication-jwt/
    - [ ] https://www.bezkoder.com/vue-3-crud/

- [ ] Tikslai page should be updated. Filter by user should be added.

- [ ] Merge red, savaitiene repo and familytool to plan features and daily tasks.
    Simply make page with a backend for storing, organizing this type of data.
    Enalbe Admin and create serveral users for all family.
    Make read book list page.

# Ways to improve this Django project example:
 - [X] Initial Django on Docker setup: https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/ 
 - [X] Create public repository and commit initial code
 - [ ] Multiple websites on the same host with lets encrypt, examples:
   - nginx has to be set up outside docker-compose: https://blog.ssdnodes.com/blog/host-multiple-ssl-websites-docker-nginx/
   - SSL and domain: https://testdriven.io/blog/django-lets-encrypt/
 - [ ] Upgrade python package versions
 - [ ] Use cookie-cutter to setup:
   - [ ] Celery
   - [ ] suggested project structure including docs
   - [ ] Authentication
 - [ ] Piptool instead of requirements.txt
 - [ ] Local development: use docker buildkit to reduce build time.
 - [ ] CI/CD on push:
    - deploy staging on push to dev branch
    - deploy production on push to production containers
 - [ ] AWS CDK setup.
 - [ ] Add some Django code.

# Steps to do:
- [ ] Write tests


