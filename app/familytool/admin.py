from django.contrib import admin

from familytool.models import User, Task


admin.site.register(User)


class TaskAdmin(admin.ModelAdmin):
    list_filter = ["user", "finished"]

admin.site.register(Task, TaskAdmin)
