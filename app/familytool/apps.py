from django.apps import AppConfig


class FamilytoolConfig(AppConfig):
    name = 'familytool'
