import json
from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        async_to_sync(self.channel_layer.group_add)("chat", self.channel_name)

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)("chat", self.channel_name)

    def receive(self, text_data):
        async_to_sync(self.channel_layer.group_send)(
            "chat",
            {
                "type": "chat.message",
                "text": "Hello, nice to meet you",
            },
        )

    def chat_message(self, event):
        self.send(text_data=event["text"])


# class ChatConsumer(WebsocketConsumer):  # RequestResponseConsumer
#     def connect(self):
#         self.accept()
#         async_to_sync(self.channel_layer.group_add)("chat", "python")
#
#     def disconnect(self, close_code):
#         async_to_sync(self.channel_layer.group_discard)("chat", "python")
#
#     def receive(self, text_data):
#         text_data_json = json.loads(text_data)
#         message = text_data_json['message']
#
#         self.send(text_data=json.dumps({
#             'message': message
#         }))
