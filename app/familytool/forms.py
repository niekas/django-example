from functools import partial
from datetime import datetime

from django import forms
from django.contrib.admin.widgets import AdminDateWidget

from familytool.models import Item, Event, Media, Todo, Task

DateInput = partial(forms.DateInput, {"class": "datepicker", "autocomplete": "off"})


class ItemCreateForm(forms.ModelForm):
    class Meta:
        model = Item
        exclude = ["bought", "expires", "already_bought"]


class EventForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        # self.initial.update({"date": str(datetime.now().today())})

    class Meta:
        model = Event
        exclude = []
        widgets = {
            "date": DateInput(),
        }


class TodoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TodoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Todo
        exclude = []
        widgets = {
            "deadline": DateInput(),
        }


class MediaForm(forms.ModelForm):
    class Meta:
        model = Media
        exclude = []


class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Task
        exclude = []
        widgets = {"deadline": DateInput()}
