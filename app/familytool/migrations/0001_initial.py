# Generated by Django 3.2.9 on 2022-02-05 12:05

import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=256, null=True)),
                ('date', models.DateTimeField()),
                ('time', models.TimeField(blank=True, null=True)),
                ('url', models.URLField(blank=True, max_length=256, null=True)),
                ('repeat_every', models.CharField(choices=[('--', '--'), ('week', 'week'), ('two_weeks', 'two_weeks'), ('month', 'month'), ('year', 'year')], default='--', max_length=40)),
                ('over', models.BooleanField()),
                ('comment', models.TextField(blank=True, default='', null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('products', models.TextField()),
                ('category', models.CharField(default='maistas', max_length=128)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=256, null=True)),
                ('type', models.CharField(choices=[('book', 'Book'), ('movie', 'Movie')], default='book', max_length=256)),
                ('url', models.URLField(blank=True, max_length=256, null=True)),
                ('got', models.BooleanField()),
                ('comment', models.TextField(blank=True, default='', null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Suggestion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128, null=True, verbose_name='Title')),
                ('slug', models.SlugField(max_length=128, verbose_name='Slug')),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField(blank=True, default='', null=True)),
                ('deadline', models.DateTimeField(blank=True, null=True)),
                ('priority', models.IntegerField(default=1, help_text='Field used for task ordering, greater - first')),
                ('date', models.DateTimeField(blank=True, help_text='At which time this task should be done', null=True)),
                ('time', models.TimeField(blank=True, help_text='At which date this task should be done', null=True)),
                ('repeat_every', models.CharField(choices=[('--', '--'), ('day', 'day'), ('week', 'week'), ('two_weeks', 'two_weeks'), ('month', 'month'), ('year', 'year'), ('workday', 'workday'), ('weekend', 'Weekend'), ('I', 'I'), ('II', 'II'), ('III', 'III'), ('IV', 'IV'), ('V', 'V'), ('VI', 'VI'), ('VII', 'VII')], default='--', help_text='How often this task should be done', max_length=40)),
                ('points', models.IntegerField(default=1, help_text='How hard is it to make this task according to Scrum point table')),
                ('url', models.URLField(blank=True, help_text='Some tasks required interaction with external tools, this field allows to save URL of external tools for easier access', max_length=256, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Todo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=256, null=True)),
                ('comment', models.TextField(blank=True, default='', null=True)),
                ('done', models.BooleanField()),
                ('deadline', models.DateTimeField(blank=True, null=True)),
                ('priority', models.IntegerField(default=1)),
                ('url', models.URLField(blank=True, max_length=256, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskDone',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('done_at', models.DateTimeField(auto_now_add=True)),
                ('points', models.IntegerField(default=1, help_text='How hard is it to do this task in points?')),
                ('task', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='done_set', to='familytool.task')),
            ],
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('engredients', models.TextField(verbose_name='Engridients')),
                ('preparation', models.TextField(verbose_name='Preparation')),
                ('url', models.URLField(blank=True, max_length=1024, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('tags', models.ManyToManyField(related_name='recipes', to='familytool.Tag')),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('comment', models.TextField(blank=True, default=None, null=True)),
                ('amount', models.DecimalField(decimal_places=3, default=1, max_digits=5)),
                ('price', models.DecimalField(decimal_places=3, default=0, max_digits=8)),
                ('already_bought', models.BooleanField(default=False)),
                ('bought', models.DateTimeField(blank=True, null=True)),
                ('expires', models.DateField(blank=True, null=True)),
                ('priority', models.IntegerField(default=1)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('unit', models.ForeignKey(default=3, null=True, on_delete=django.db.models.deletion.CASCADE, to='familytool.unit')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, primary_key=True, serialize=False, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
