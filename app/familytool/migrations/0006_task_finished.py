# Generated by Django 3.2.9 on 2022-03-02 07:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('familytool', '0005_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='finished',
            field=models.BooleanField(default=False),
        ),
    ]
