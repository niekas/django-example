from datetime import datetime
from random import randint
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.validators import UnicodeUsernameValidator

from familytool.utils import ChoiceArrayField


REPEAT_EVERY = (
    ("--", _("--")),
    ("day", _("day")),
    ("week", _("week")),
    ("two_weeks", _("two_weeks")),
    ("month", _("month")),
    ("year", _("year")),
    ("workday", _("workday")),
    ("weekend", _("Weekend")),
    ("I", _("I")),
    ("II", _("II")),
    ("III", _("III")),
    ("IV", _("IV")),
    ("V", _("V")),
    ("VI", _("VI")),
    ("VII", _("VII")),
)


class Tag(models.Model):
    title = models.CharField(_("Title"), max_length=128, null=True)
    slug = models.SlugField(_("Slug"), max_length=128)
    created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("recipe-list")

    def __str__(self):
        return self.title or self.slug


class Recipe(models.Model):
    title = models.CharField(_("Title"), max_length=255)
    engredients = models.TextField(_("Engridients"))
    preparation = models.TextField(_("Preparation"))
    tags = models.ManyToManyField("Tag", related_name="recipes")
    url = models.URLField(max_length=1024, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("recipe-list")

    def __str__(self):
        return self.title or "Recipe object"

class Unit(models.Model):
    title = models.CharField(max_length=128)
    created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("recipe-list")

    def __str__(self):
        return self.title


class Item(models.Model):
    title = models.CharField(max_length=128)
    comment = models.TextField(null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=5, decimal_places=3, default=1)
    price = models.DecimalField(max_digits=8, decimal_places=3, default=0)
    unit = models.ForeignKey("Unit", null=True, default=3, on_delete=models.CASCADE)
    already_bought = models.BooleanField(default=False)
    bought = models.DateTimeField(null=True, blank=True)
    expires = models.DateField(null=True, blank=True)
    priority = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("item-list")

    def __str__(self):
        return "%s, %g %s" % (self.title, self.amount, self.unit)

    def save_as_bougth(self, expires=None):
        self.already_bought = True
        self.bought = datetime.now()
        self.amount = str(self.amount)
        if expires:
            self.expires = expires
        self.save()


class Expense(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    products = models.TextField()
    category = models.CharField(max_length=128, default="maistas")
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created"]

    def get_absolute_url(self):
        return reverse("expense-list")

    def __str__(self):
        return "%.2f lt - %s" % (self.amount, self.products)


class Suggestion(models.Model):
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return self.comment

    def get_absolute_url(self):
        return reverse("suggestions")


class Event(models.Model):
    DURATIONS = (
        ("--", _("--")),
        ("week", _("week")),
        ("two_weeks", _("two_weeks")),
        ("month", _("month")),
        ("year", _("year")),
    )
    title = models.CharField(max_length=256, null=True, blank=True)
    date = models.DateTimeField()
    time = models.TimeField(null=True, blank=True)
    url = models.URLField(max_length=256, null=True, blank=True)
    repeat_every = models.CharField(max_length=40, choices=DURATIONS, default="--")   # icon-repeat
    over = models.BooleanField()
    comment = models.TextField(null=True, blank=True, default="")
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("events")


class Todo(models.Model):
    title = models.CharField(max_length=256, null=True, blank=True)
    comment = models.TextField(null=True, blank=True, default="")

    done = models.BooleanField()

    deadline = models.DateTimeField(null=True, blank=True)
    priority = models.IntegerField(default=1)
    url = models.URLField(max_length=256, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("todos")


class Media(models.Model):
    MEDIA_TYPES = (
        ("book", _("Book")),
        ("movie", _("Movie")),
    )
    title = models.CharField(max_length=256, null=True, blank=True)
    type = models.CharField(max_length=256, choices=MEDIA_TYPES, default="book")
    url = models.URLField(max_length=256, null=True, blank=True)
    got = models.BooleanField()
    comment = models.TextField(null=True, blank=True, default="")
    created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse("media-list")


class User(AbstractUser):
    username = models.CharField(
        _('username'),
        max_length=150,
        primary_key=True,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[UnicodeUsernameValidator()],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )

    def __str__(self):
        return f"{self.first_name or self.username or self.email}"


TASK_TYPES = (
    ("discipline", "discipline"),
    ("feature", "feature"),
)

class Task(models.Model):
    content = models.TextField(null=True, blank=True, default="")

    finished = models.BooleanField(default=False)
    type = models.CharField(max_length=40, choices=TASK_TYPES, default="discipline")

    deadline = models.DateTimeField(null=True, blank=True)
    priority = models.IntegerField(default=1, help_text="Field used for task ordering, greater - first")

    date = models.DateTimeField(null=True, blank=True, help_text="At which time this task should be done")
    time = models.TimeField(null=True, blank=True, help_text="At which date this task should be done")

    repeat_every = models.CharField(max_length=40, choices=REPEAT_EVERY, default="--", help_text=(
        "How often this task should be done"))

    frequency = ChoiceArrayField(models.CharField(max_length=90, choices=REPEAT_EVERY), blank=True, default=list)

    points = models.IntegerField(default=1, help_text=(
        "How hard is it to make this task according to Scrum point table"))
    # When showing tasks, check task history if this criteria is met.

    url = models.URLField(max_length=256, null=True, blank=True, help_text=(
        "Some tasks required interaction with external tools, this field allows to "
        "save URL of external tools for easier access"))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,)

    def __str__(self):
        return self.content

    def get_absolute_url(self):
        return reverse("task-edit")


class TaskDone(models.Model):
    task = models.ForeignKey(Task, null=True, on_delete=models.CASCADE, related_name="done_set")
    done_at = models.DateTimeField(auto_now_add=True)
    points = models.IntegerField(default=1, help_text="How hard is it to do this task in points?")
    user = models.ForeignKey(User, on_delete=models.CASCADE)


PRODUCT_CATEGORIES = (
    ("maistas", _("Maistas")),
)

class Product(models.Model):
    name = models.CharField(max_length=128, default="maistas")
    image = models.ImageField(upload_to="product_images")
    category = models.CharField(max_length=128, choices=PRODUCT_CATEGORIES, default="maistas")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


def generate_account_serial_number(*args, **kwargs):
    # TODO: collisions might occour if the same number is generated.
    #       Serial should be used instead.
    return f"acc{randint(1000000, 9999999)}"


class AccountLog(models.Model):
    diff = models.DecimalField(max_digits=20, decimal_places=2)
    reason = models.TextField(_("Comment"))
    account = models.ForeignKey("MoneyAccount", on_delete=models.CASCADE, related_name="logs")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created_at"]


class MoneyAccount(models.Model):
    id = models.CharField(max_length=512, default=generate_account_serial_number, primary_key=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def add(self, diff: float, reason: str):
        AccountLog.objects.create(account=self, diff=diff, reason=reason)
        self.amount += Decimal(diff)
        self.save(update_fields=["amount"])

    def __repr__(self):
        return f"{self.id} {self.amount} {self.name} - {self.user.username}"

    class Meta:
        ordering = ["created_at"]

    DURATION_IN_MONTHS = {
        "year": 12,
        "month": 1,
        "3months": 3,
        "6months": 6,
        "9months": 9,

    }
    DURATION_TO_INTEREST_RATE = {
        "year": 4.3,
        "month": 2.0 ,
        "3months": 3.0,
        "6months": 3.5,
        "9months": 4.0,
    }

    def deposit(self, duration: str, amount: float):
        interest_rate = self.DURATION_TO_INTEREST_RATE[duration]
        months = self.DURATION_IN_MONTHS[duration]
        amount_with_interest = float(amount) * ((100. + interest_rate) / 100.) * (months / 12.)
        ends_at = (datetime.now() + relativedelta(months=months))
        
        deposit = SavingsDeposit.objects.create(
            account=self,
            amount=amount,
            duration=duration,
            amount_with_interest=amount_with_interest,
            interest_rate=interest_rate,
            ends_at=ends_at,
        )
        self.add(-1 * amount, f"Įdėtas indėlis {amount}€ su palūkanom {interest_rate}%, kuris baigiasi {ends_at.isoformat(timespec='seconds').replace('T',' ')} (ID={deposit.id})")

        # duration = models.CharField(max_length=512, choices=DURATIONS)
        # duration: str, amount: float

        # TODO: create account log, which includes interest rate, end date and final amount.


class SavingsDeposit(models.Model):
    DURATIONS = (
        ("year", _("metai")),
        ("month", _("mėnuo")),
        ("3months", _("3 mėnesiai")),
        ("6months", _("6 mėnesiai")),
        ("9months", _("9 mėnesiai")),
    )

    account = models.ForeignKey("MoneyAccount", on_delete=models.CASCADE, related_name="deposits")
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    amount_with_interest = models.DecimalField(max_digits=20, decimal_places=2)
    duration = models.CharField(max_length=512, choices=DURATIONS)
    interest_rate = models.DecimalField(max_digits=20, decimal_places=2)
    starts_at = models.DateTimeField(auto_now_add=True)
    ends_at = models.DateTimeField()
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def terminate(self):
        self.account.add(self.amount, f"Nutrauktas indėlis {self.amount}€ (palūkanos {self.interest_rate}% nepriklauso, nes nesulauktas pabaigos terminas {self.ends_at.isoformat(timespec='seconds').replace('T',' ').replace('+00:00','')}) (ID={self.id})")
        self.delete()

    def update(self):
        if self.ends_at.replace(tzinfo=None) < datetime.now().replace(tzinfo=None):
            ends_at = self.ends_at.replace(tzinfo=None).isoformat(timespec='seconds').replace('T',' ')
            self.account.add(self.amount_with_interest, f"Indėlis {self.amount}€ pasibaigė su palūkanom {self.interest_rate}% {ends_at}, išmokėta {self.amount_with_interest}€ (ID={self.id})")
        self.delete()








