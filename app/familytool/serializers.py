from datetime import datetime

from rest_framework import serializers

from familytool.models import Task, TaskDone


class TaskSerializer(serializers.ModelSerializer):
    done = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = [
            "id",
            "content",
            "type",
            "deadline",
            "priority",
            "date",
            "time",
            "repeat_every",
            "points",
            "url",
            "created_at",
            "updated_at",
            "user",
            "done",
        ]

    def get_done(self, obj):

        if obj.done_set.filter(done_at__date=datetime.now().date()) or (
            obj.frequency == [] and obj.done_set.count()
        ):
            return True
        return False


class TaskDoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskDone
        fields = "__all__"
