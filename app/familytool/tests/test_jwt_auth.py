from django.urls import reverse
from model_bakery.baker import make
from rest_framework.test import APITestCase
from rest_framework import status


class JWTAuthenticationAPITestCase(APITestCase):
    def setUp(self):
        self.data = {
            "username": "test_user",
            "password": "test_secret",
        }
        self.user = make("familytool.User", username=self.data["username"])
        self.user.set_password(self.data["password"])
        self.user.save()

    def test_jwt_authentication(self):
        url = reverse("token-obtain-pair")
        resp = self.client.post(url, data=self.data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn("access", resp.json())
        self.assertIn("refresh", resp.json())

    def test_jwt_token_refresh(self):
        url = reverse("token-obtain-pair")
        resp = self.client.post(url, data=self.data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn("refresh", resp.json())

        url = reverse("token-refresh")
        resp = self.client.post(url, data={"refresh": resp.json()["refresh"]})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn("access", resp.json())
