from django.urls import reverse
from model_bakery.baker import make
from rest_framework.test import APITestCase
from rest_framework import status

from familytool.models import Task, TaskDone, User


class TestListPagesTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create(username="Alex")
        self.client.force_authenticate(user=user)

        self.task1 = make(Task, _fill_optional=True)
        self.task2 = make(Task, _fill_optional=True)

    def test_get_pages(self):
        page_names = [
            "index",
            "recipe-list",
            "recipe-create",

            "item-create",
            "item-list",

            "expense-list",
            "expense-create",

            "suggestions",

            "events",

            "todos",

            "media-list",

            "task-create",
            "task-list",

            "user-list"]

        for page_name in page_names:
            resp = self.client.get(reverse(page_name))
            self.assertEqual(resp.status_code, 200)

    def test_task_list_page(self):
        resp = self.client.get(reverse("task-list") + "?user=Alex")
        self.assertEqual(resp.status_code, 200)
