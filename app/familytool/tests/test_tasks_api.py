from django.urls import reverse
from model_bakery.baker import make
from rest_framework.test import APITestCase
from rest_framework import status

from familytool.models import Task, TaskDone, User


class TasksAPITestCase(APITestCase):
    def setUp(self):
        user = User.objects.create(username="Alex")
        self.client.force_authenticate(user=user)

        self.task1 = make(Task, _fill_optional=True)
        self.task2 = make(Task, _fill_optional=True)

    def test_get_task_list(self):
        resp = self.client.get(reverse("api-task-list"))

        self.assertEqual(len(resp.json()), 2)
        self.assertEqual(resp.json()[0]["content"], self.task1.content)
        self.assertEqual(resp.json()[0]["points"], self.task1.points)

        self.assertEqual(resp.json()[1]["content"], self.task2.content)
        self.assertEqual(resp.json()[1]["points"], self.task2.points)

    def test_get_task_list_filter(self):
        resp = self.client.get(reverse("api-task-list") + "?user=laura")

        # TODO: add filtering response should be 0
        self.assertEqual(len(resp.json()), 2)

    def test_get_task_detail(self):
        resp = self.client.get(reverse("api-task-detail", kwargs={"pk": self.task1.pk}))

        self.assertEqual(resp.json()["content"], self.task1.content)
        self.assertEqual(resp.json()["points"], self.task1.points)

    def test_task_create(self):
        data = {
            "content": "My example task",
            "date": "2021-12-15T11:56:00.148416Z",
            "deadline": "2021-12-15T11:56:00.147437Z",
            "id": 1,
            "points": 1,
            "priority": 1,
            "repeat_every": "--",
            "time": "11:56:00.147455",
            "url": "http://www.example.com/",
            "user": "Alex"
        }

        resp = self.client.post(reverse("api-task-list"), data=data)
        self.assertEqual(resp.json()["content"], data["content"])
        self.assertEqual(resp.json()["date"], data["date"])
        self.assertEqual(resp.json()["deadline"], data["deadline"])
        self.assertEqual(resp.json()["points"], data["points"])
        self.assertEqual(resp.json()["priority"], data["priority"])
        self.assertEqual(resp.json()["repeat_every"], data["repeat_every"])
        self.assertEqual(resp.json()["time"], data["time"])
        self.assertEqual(resp.json()["url"], data["url"])
        self.assertEqual(resp.json()["user"], data["user"])

    def test_task_update(self):
        data = {
            ""
            "content": "My example task",
            "date": "2021-12-15T11:56:00.148416Z",
            "deadline": "2021-12-15T11:56:00.147437Z",
            "id": 1,
            "points": 1,
            "priority": 1,
            "repeat_every": "--",
            "time": "11:56:00.147455",
            "url": "http://www.example.com/",
            "user": "Alex",
        }

        resp = self.client.put(reverse("api-task-detail", kwargs={"pk": self.task1.pk}), data=data)
        self.assertEqual(resp.json()["content"], data["content"])
        self.assertEqual(resp.json()["date"], data["date"])
        self.assertEqual(resp.json()["deadline"], data["deadline"])
        self.assertEqual(resp.json()["points"], data["points"])
        self.assertEqual(resp.json()["priority"], data["priority"])
        self.assertEqual(resp.json()["repeat_every"], data["repeat_every"])
        self.assertEqual(resp.json()["time"], data["time"])
        self.assertEqual(resp.json()["url"], data["url"])
        self.assertEqual(resp.json()["user"], data["user"])

    def test_task_delete(self):
        task = make(Task, _fill_optional=True)
        resp = self.client.delete(reverse("api-task-detail", kwargs={"pk": task.pk}))
        self.assertTrue(resp.status_code, status.HTTP_204_NO_CONTENT)

    def test_task_done(self):
        self.assertEqual(TaskDone.objects.count(), 0)

        resp = self.client.post(reverse("api-task-done", kwargs={"pk": self.task1.pk}))
        self.assertEqual(resp.status_code, 200)

        self.assertTrue(resp.json()["done_at"])
        self.assertEqual(resp.json()["id"], 1)
        self.assertEqual(resp.json()["points"], 1)
        self.assertEqual(resp.json()["task"], self.task1.pk)
        self.assertEqual(resp.json()["user"], "Alex")

        self.assertEqual(TaskDone.objects.count(), 1)
