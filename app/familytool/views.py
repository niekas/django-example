from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta  # noqa
import json
from django.contrib import messages

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import Request
from rest_framework_simplejwt.tokens import RefreshToken

from django.core import serializers
from django.contrib.auth import get_user
from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db import transaction
from django.urls import reverse, reverse_lazy
from django.utils.html import mark_safe
from django.utils import timezone
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import csrf_exempt

from familytool.models import Event, Expense, Item, Media, Recipe, Suggestion, Tag, Todo, Unit, User, Task, MoneyAccount, SavingsDeposit, TaskDone
from familytool.forms import ItemCreateForm, EventForm, MediaForm, TodoForm, TaskForm


@api_view(["GET"])
def index(request: Request, *args, **kwargs):
    if request.get_host().startswith("red."):
        return HttpResponseRedirect(redirect_to=reverse("red"))
    return HttpResponseRedirect(redirect_to=reverse("task-list"))


@api_view()
def red(request: Request, *args, **kwargs):
    return render(request, "familytool/red.html", {})


class RecipeList(ListView):
    model = Recipe

    def get_queryset(self):
        qs = super().get_queryset().order_by("-created")
        tag_slug = self.kwargs.get("slug")
        if tag_slug:
            qs = qs.filter(tags__slug=tag_slug)
        return qs

    def get_context_data(self, **kwargs):
        context = super(RecipeList, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context


class RecipeCreate(CreateView):
    model = Recipe
    fields = ["title", "engredients", "preparation", "url"]


class RecipeUpdate(UpdateView):
    model = Recipe
    fields = ["title", "engredients", "preparation", "url", "tags"]


class RecipeDelete(DeleteView):
    model = Recipe
    success_url = reverse_lazy("recipe-list")


class TagCreate(CreateView):
    model = Tag
    template_name = "familytool/obj_form.html"
    fields = ['title', 'slug']


class TagUpdate(UpdateView):
    model = Tag
    fields = ['title', 'slug']


class TagDelete(DeleteView):
    model = Tag
    success_url = reverse_lazy('recipe-list')


class UnitCreate(CreateView):
    model = Unit
    fields = ['title']
    template_name = "familytool/obj_form.html"


class UnitDelete(DeleteView):
    model = Unit
    success_url = reverse_lazy('recipe-list')


class ExpenseList(ListView):
    model = Expense
    paginate_by = 30

    def get_context_data(self, **kwargs):
        context = super(ExpenseList, self).get_context_data(**kwargs)
        context.update(Expense.objects.filter(
            created__gt=datetime.now() - relativedelta(months=1), category="maistas"
        ).aggregate(month_food_expense=Sum('amount')))
        context.update(Expense.objects.filter(
            created__gt=datetime.now() - relativedelta(months=1)
        ).aggregate(month_expense=Sum('amount')))
        context.update(Expense.objects.aggregate(total_expense=Sum('amount')))
        return context


class ExpenseCreate(CreateView):
    model = Expense
    template_name = "familytool/obj_form.html"
    fields = ["amount", "products", "category"]

    def get_context_data(self, **kwargs):
        context = super(ExpenseCreate, self).get_context_data(**kwargs)
        context['active'] = 'add-expense'
        return context


class ExpenseUpdate(UpdateView):
    model = Expense
    template_name = "familytool/obj_form.html"


class ExpenseDelete(DeleteView):
    model = Expense
    success_url = reverse_lazy('expense-list')


def item_priority_increase(request, pk):
    item = Item.objects.get(pk=pk)
    item.priority += 1
    item.save()
    return HttpResponseRedirect(reverse_lazy('item-list'))


def item_bought(request, pk):
    item = Item.objects.get(pk=pk)
    item.save_as_bougth(expires=request.POST.get('expires'))
    return HttpResponseRedirect(reverse_lazy('item-list'))


class ItemCreate(CreateView):
    model = Item
    form_class = ItemCreateForm
    template_name = "familytool/obj_form.html"

    def get_context_data(self, **kwargs):
        context = super(ItemCreate, self).get_context_data(**kwargs)
        context['active'] = 'add-item'
        return context


class ItemList(ListView):
    model = Item

    def get_queryset(self):
        qs = super(ItemList, self).get_queryset()
        if not self.kwargs.get('bought'):
            qs = qs.filter(already_bought=False)
        qs = qs.order_by('-priority', '-created')
        return qs


class ItemUpdate(UpdateView):
    model = Item
    fields = ['title', 'comment', 'amount', 'unit', 'price', 'already_bought', 'expires', 'priority']


class ItemDelete(DeleteView):
    model = Item
    success_url = reverse_lazy('item-list')


@api_view()
def subscriptions(request: Request, *args, **kwargs):
    request.user = get_user(request)
    return render(request, "familytool/subscriptions.html", {})


class SuggestionCreate(CreateView):
    model = Suggestion
    fields = ['comment']
    template_name = "familytool/suggestion_form.html"

    def get_context_data(self, **kwargs):
        context = super(SuggestionCreate, self).get_context_data(**kwargs)
        context['obj_list'] = Suggestion.objects.all()
        return context


class SuggestionUpdate(UpdateView):
    model = Suggestion
    fields = ['comment']
    template_name = "familytool/obj_form.html"


class SuggestionDelete(DeleteView):
    model = Suggestion
    success_url = reverse_lazy('suggestions')


class EventCreate(CreateView):
    model = Event
    template_name = "familytool/event_form.html"
    form_class = EventForm

    def get_context_data(self, **kwargs):
        context = super(EventCreate, self).get_context_data(**kwargs)
        context['events'] = Event.objects.filter(over=False).order_by('date', 'time')
        context['today'] = datetime.today().date()
        context['tomorrow'] = datetime.today().date() + timedelta(1)
        if self.request.GET.get('all'):
            context['events'] = Event.objects.all().order_by('date', 'time')
        context['active'] = 'events'
        return context


class EventUpdate(UpdateView):
    model = Event
    fields = ['title', 'comment', 'date', 'time', 'url', 'repeat_every', 'over']
    template_name = "familytool/event_update.html"


class EventDelete(DeleteView):
    model = Event
    success_url = reverse_lazy('events')


def event_over(request, pk):
    if request.method == 'POST':
        event = Event.objects.get(pk=pk)
        event.over = True
        event.save()
    return HttpResponseRedirect(reverse_lazy('events'))


def repeat_event(request, pk):
    if request.method == 'POST':
        event = Event.objects.get(pk=pk)
        if event.repeat_every == 'week':
            event.date = event.date + timedelta(7)
        elif event.repeat_every == 'two_weeks':
            event.date = event.date + timedelta(14)
        elif event.repeat_every == 'month':
            event.date = event.date + relativedelta(months=1)
        elif event.repeat_every == 'year':
            event.date = event.date + relativedelta(years=1)
        event.save()
    return HttpResponseRedirect(reverse_lazy('events'))


class TodoCreate(CreateView):
    model = Todo
    template_name = "familytool/todo_form.html"
    form_class = TodoForm

    def get_context_data(self, **kwargs):
        context = super(TodoCreate, self).get_context_data(**kwargs)
        context['todos'] = Todo.objects.filter(done=False).order_by('-priority')
        context['today'] = datetime.today().date()
        if self.request.GET.get('all'):
            context['todos'] = Todo.objects.all().order_by('-priority')
        context['active'] = 'todos'
        return context


def todo_priority_increase(request, pk):
    todo = Todo.objects.get(pk=pk)
    todo.priority += 1
    todo.save()
    return HttpResponseRedirect(reverse_lazy('todos'))


def todo_priority_decrease(request, pk):
    todo = Todo.objects.get(pk=pk)
    todo.priority -= 1
    todo.save()
    return HttpResponseRedirect(reverse_lazy('todos'))


# TODO: remove this method in favor of TaskViewSet.done
def todo_done(request, pk):
    todo = Todo.objects.get(pk=pk)
    todo.done = True
    todo.save()
    return HttpResponseRedirect(reverse_lazy('todos'))


class TodoUpdate(UpdateView):
    model = Todo
    fields = ['title', 'deadline', 'priority', 'url', 'done', 'comment']
    template_name = "familytool/todo_update.html"


class TodoDelete(DeleteView):
    model = Todo
    success_url = reverse_lazy('todos')


class MediaCreate(CreateView):
    model = Media
    template_name = "familytool/media_form.html"
    form_class = MediaForm

    def get_context_data(self, **kwargs):
        context = super(MediaCreate, self).get_context_data(**kwargs)
        context['media_list'] = Media.objects.all().order_by('created')
        return context


class MediaUpdate(UpdateView):
    model = Media
    fields = ['title', 'type', 'url', 'got', 'comment']
    template_name = "familytool/media_update.html"


class MediaDelete(DeleteView):
    model = Media
    success_url = reverse_lazy('media-list')


class UserListView(ListView):
    """Shows registered users"""

    model = User
    template_name = 'familytool/objects.html'


class TodoListView(ListView):
    """Track todo tasks?"""

    model = Todo
    template_name = 'familytool/objects.html'


# Tasks
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from familytool.serializers import TaskSerializer, TaskDoneSerializer


def mark_task_done(request, pk):
    today = timezone.now().replace(hour=0, minute=0, second=0)
    user = get_user(request)
    task = Task.objects.get(pk=pk)
    if not TaskDone.objects.filter(task=task, user=user, done_at__gte=today).exists():
        TaskDone.objects.create(task=task, user=user)
        tasks_done = Task.objects.filter(user=user, finished=False, done_set__done_at__gte=today).count()
        tasks_to_do = Task.objects.filter(user=user, finished=False).count()
        all_tasks_finished = tasks_done == tasks_to_do
        if all_tasks_finished: 
            account = MoneyAccount.objects.get(user=user)
            account.add(1, "Atliktos visos dienos užduotys")
    return HttpResponseRedirect(redirect_to=reverse("task-list"))


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]



    @action(detail=True, methods=["post"])
    def done(self, request, pk=None):
        task = self.get_object()
        task_data = {
            "task": pk,
            "user": request.user.pk,
            "points": task.points,
            **request.POST
        }

        if task.type == "feature":
            task_data["finished"] = True

        task_done_serializer = TaskDoneSerializer(data=task_data)
        task_done_serializer.is_valid(raise_exception=True)
        task_done_serializer.save()
        return Response(task_done_serializer.data)


# TODO: add logging for better debuging

class TaskListView(ListView):
    model = Task
    template_name = "familytool/tasks.html"

    def get_queryset(self):
        user_pk = get_user(self.request).pk or self.request.user.pk
        # user_pk = self.request.GET.get("user") or 

        weekday = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'][datetime.now().weekday()]
        workday = "workday" if weekday in ['VI', 'VII'] else "weekend"

        return super().get_queryset().filter(
            Q(finished=False),
            Q(frequency__overlap=[
            weekday, workday, "--", "day", "week", "two_weeks", "month", "year"
        ]) | Q(frequency__len=0), user=user_pk).order_by("priority", "-created_at")

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        # Should call api-list-task to get task-list, including filtering.

        # context["tasks"] = mark_safe(serializers.serialize("json", self.get_queryset()))
        # context["tasks"] = mark_safe(json.dumps(TaskSerializer(self.get_queryset(), many=True).data))
        context["tasks"] = self.get_queryset()
        context["users"] = User.objects.order_by("username")
        context["api_token"] = mark_safe(RefreshToken.for_user(self.request.user).access_token)
        return context


class TaskCreate(CreateView):
    model = Task
    template_name = "familytool/tasks.html"
    form_class = TaskForm


class TaskUpdate(UpdateView):
    model = Task
    fields = ["content", "deadline", "priority", "date", "time", "repeat_every", "points", "url", "user"]
    template_name = "familytool/task_update.html"


class TaskDelete(DeleteView):
    model = Task
    success_url = reverse_lazy('task-list')



class MoneyAccountCreate(CreateView):
    model = MoneyAccount
    template_name = "familytool/accounts.html"


class MoneyAccountListView(ListView):
    model = MoneyAccount
    template_name = "familytool/accounts.html"


class MoneyAccountDetailView(DetailView):
    model = MoneyAccount
    template_name = "familytool/accounts_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for deposit in self.object.deposits.all():
            deposit.update()
        return context


class MoneyAccountUpdate(UpdateView):
    model = MoneyAccount
    fields = ["name", "amount", "user"]
    template_name = "familytool/account_update.html"


class MoneyAccountDelete(DeleteView):
    model = MoneyAccount
    success_url = reverse_lazy('account-list')


@api_view(["POST"])
def create_savings_deposit(request: Request, *args, **kwargs):
    request.user = get_user(request)
    account_pk = kwargs.get("account_pk")
    account = MoneyAccount.objects.get(pk=account_pk)

    if account.user != request.user:
        messages.add_message(request, messages.WARNING, f"Nepakanka teisių {account.user} != {request.user}")
        return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))

    amount = request.POST["amount"]
    if not amount.isdigit():
        messages.add_message(request, messages.WARNING,
                             f"Netinkama suma: {amount}€")
        return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))

    if float(amount) < 10:
        messages.add_message(request, messages.WARNING,
                             f"Per maža suma: {float(amount):.02f}€ < 10.00€")
        return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))

    if account.amount < float(amount):
        messages.add_message(request, messages.WARNING,
                             f"Nepakanka pinigų: turimi {account.amount}€ < {float(amount)}€")
        return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))

    duration = request.POST["duration"]
    with transaction.atomic():
        deposit = account.deposit(duration, float(amount))
    # raise ValueError(f"Deposit was created {deposit} {deposit.id}")
    return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))


@api_view(["POST"])
@transaction.atomic
def terminate_savings_deposit(request: Request, *args, **kwargs):
    if request.method == "POST":
        request.user = get_user(request)
        savings_deposit = SavingsDeposit.objects.get(pk=kwargs.get("pk"))
        if savings_deposit.account.user != request.user:
            messages.add_message(request, messages.WARNING, f"Nepakanka teisių: {savings_deposit.account.user} != {request.user}")
            return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": account_pk}))

        savings_deposit.terminate()
    return HttpResponseRedirect(redirect_to=reverse("accounts-detail", kwargs={"pk": savings_deposit.account.pk}))


def robots(request):
    return HttpResponse("User-agent: * Disallow: /")


@csrf_exempt
def dns_tunnel_view(request):
    if request.method == "POST":
        from familytool.utils.refresh_ips import refresh_ips

        hosts_file_content = request.body.decode()
        updated_hosts_file_content = refresh_ips(hosts_file_content)

        return HttpResponse(updated_hosts_file_content)
    return HttpResponse('')
