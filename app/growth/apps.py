from django.apps import AppConfig


class GrowthConfig(AppConfig):
    """Flow shown at: https://miro.com/app/board/uXjVPyURG9w=/

    This app should consit of four main parts:
        - Tasks
        - Habits
        - Visions
        - Skills
    """

    default_auto_field = 'django.db.models.UUIDField'
    name = 'growth'
