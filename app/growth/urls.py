from django.urls import path

from . import views

urlpatterns = [
    path('tasks/', views.tasks),
    path('habits/', views.habits),
    path('visions/', views.visions),
    path('skills/', views.skills),
] 
