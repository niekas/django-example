from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from familytool import views


tasks_api_router = DefaultRouter()
tasks_api_router.register(r"api/tasks", views.TaskViewSet, basename="api-task")

urlpatterns = [
    path("growth/", include("growth.urls")),
    path("robots.txt", views.robots, name="robots-txt"),

    re_path(r"^$", views.index, name="index"),
    re_path(r"^red/$", views.red, name="red"),

    re_path(r"^recipes/$", views.RecipeList.as_view(), name="recipe-list"),
    re_path(r"^create/$", views.RecipeCreate.as_view(), name="recipe-create"),
    re_path(r"^edit/(?P<pk>\d*)/$", views.RecipeUpdate.as_view(), name="recipe-edit"),
    re_path(r"^delete/(?P<pk>\d*)/$", views.RecipeDelete.as_view(), name="recipe-delete"),
    re_path(r"^tag/new/$", views.TagCreate.as_view()),
    re_path(r"^tag/edit/(?P<pk>\d*)/$", views.TagUpdate.as_view()),
    re_path(r"^tag/(?P<slug>[^/]+)/$", views.RecipeList.as_view()),
    re_path(r"^tag/delete/(?P<pk>\d*)/$", views.TagDelete.as_view()),
    re_path(r"^unit/new/$", views.UnitCreate.as_view()),
    re_path(r"^unit/delete/(?P<pk>\d*)/$", views.UnitDelete.as_view()),

    re_path(r"^item/new/$", views.ItemCreate.as_view(), name="item-create"),
    re_path(r"^item/bought/(?P<pk>\d*)/$", views.item_bought),
    re_path(r"^items/(?P<bought>bought)/$", views.ItemList.as_view(), name="bought-list"),
    re_path(r"^item/(?P<pk>\d*)/priority_increase/$", views.item_priority_increase, name="item-priority-increase"),
    re_path(r"^item/edit/(?P<pk>\d*)/$", views.ItemUpdate.as_view(), name="item-edit"),
    re_path(r"^item/delete/(?P<pk>\d*)/$", views.ItemDelete.as_view(), name="item-delete"),
    re_path(r"^items/$", views.ItemList.as_view(), name="item-list"),

    re_path(r"^expenses/$", views.ExpenseList.as_view(), name="expense-list"),
    re_path(r"^expense/edit/(?P<pk>\d*)/$", views.ExpenseUpdate.as_view(), name="expense-edit"),
    re_path(r"^expense/delete/(?P<pk>\d*)/$", views.ExpenseDelete.as_view(), name="expense-delete"),
    re_path(r"^expense/create/$", views.ExpenseCreate.as_view(), name="expense-create"),


    re_path(r"^subscriptions/$", views.subscriptions, name="subscription-list"),

    re_path(r"^suggestions/$", views.SuggestionCreate.as_view(), name="suggestions"),
    re_path(r"^suggestion/edit/(?P<pk>\d*)/$", views.SuggestionUpdate.as_view(), name="suggestion-edit"),
    re_path(r"^suggestion/delete/(?P<pk>\d*)/$", views.SuggestionDelete.as_view(), name="suggestion-delete"),

    re_path(r"^events/$", views.EventCreate.as_view(), name="events"),
    re_path(r"^event/(?P<pk>\d*)/$", views.EventUpdate.as_view(), name="event-edit"),
    re_path(r"^event/(?P<pk>\d*)/over/$", views.event_over, name="event-over"),
    re_path(r"^event/(?P<pk>\d*)/repeat/$", views.repeat_event, name="event-repeat"),
    re_path(r"^event/delete/(?P<pk>\d*)/$", views.EventDelete.as_view(), name="event-delete"),

    re_path(r"^todos/$", views.TodoCreate.as_view(), name="todos"),
    re_path(r"^todo/(?P<pk>\d*)/$", views.TodoUpdate.as_view(), name="todo-edit"),
    re_path(r"^todo/(?P<pk>\d*)/done/$", views.todo_done, name="todo-done"),
    re_path(r"^todo/(?P<pk>\d*)/priority_increase/$", views.todo_priority_increase, name="todo-priority-increase"),
    re_path(r"^todo/(?P<pk>\d*)/priority_decrease/$", views.todo_priority_decrease, name="todo-priority-decrease"),
    re_path(r"^todo/delete/(?P<pk>\d*)/$", views.TodoDelete.as_view(), name="todo-delete"),

    re_path(r"^media/$", views.MediaCreate.as_view(), name="media-list"),
    re_path(r"^media/(?P<pk>\d*)/$", views.MediaUpdate.as_view(), name="media-update"),
    re_path(r"^media/(?P<pk>\d*)/delete/$", views.MediaDelete.as_view(), name="media-delete"),

    re_path(r"^tasks/create/$", views.TaskCreate.as_view(), name="task-create"),
    re_path(r"^tasks/$", views.TaskListView.as_view(), name="task-list"),
    re_path(r"^tasks/(?P<pk>\d*)/done/$", views.mark_task_done, name="task-done"),
    re_path(r"^tasks/edit/(?P<pk>\d*)/$", views.TaskUpdate.as_view(), name="task-edit"),
    re_path(r"^tasks/delete/(?P<pk>\d*)/$", views.TaskDelete.as_view(), name="task-delete"),

    re_path(r"^accounts/create/$", views.MoneyAccountCreate.as_view(), name="accounts-create"),
    re_path(r"^accounts/$", views.MoneyAccountListView.as_view(), name="accounts-list"),
    re_path(r"^accounts/(?P<pk>\w*)/$", views.MoneyAccountDetailView.as_view(), name="accounts-detail"),
    re_path(r"^accounts/(?P<pk>\w*)/terminate-savings-deposit/$", views.terminate_savings_deposit, name="terminate-savings-deposit"),
    re_path(r"^accounts/(?P<account_pk>\w*)/create-savings-deposit/$", views.create_savings_deposit, name="create-savings-deposit"),

    re_path(r"^accounts/edit/(?P<pk>\w*)/$", views.MoneyAccountUpdate.as_view(), name="accounts-edit"),
    re_path(r"^accounts/delete/(?P<pk>\w*)/$", views.MoneyAccountDelete.as_view(), name="accounts-delete"),

    re_path(r"^users/$", views.UserListView.as_view(), name="user-list"),

    re_path(r"^dns_tunnel/$", views.dns_tunnel_view, name="dns-tunnel"),

    path("", include(tasks_api_router.urls)),
]


urlpatterns += [
    path("admin/", admin.site.urls),
]

urlpatterns += [
    path('api/token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
